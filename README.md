**Vehicle Detection**
[![Udacity - Self-Driving Car NanoDegree](https://s3.amazonaws.com/udacity-sdc/github/shield-carnd.svg)](http://www.udacity.com/drive)
---
![SSD Sample](/output_images/ssd_sample.gif)

The primary goal of the project is the identification and position estimation of vehicles.

Two approaches were adopted in this project, with varied degrees of success:
* Feature extraction in conjunction with a Linear [Support Vector Classifier (SVC)](http://image.diku.dk/imagecanon/material/cortes_vapnik95.pdf) for detection, ad-hoc temporal based methods for false positive suppression
* [Single Shot MultiBox Detector (SSD)](https://arxiv.org/pdf/1512.02325.pdf)  for detection, confidence thresholding and temporal based methods for false positive suppression

In order to run the SVC notebooks, you will need:
* the Udacity labelled training [data](https://s3.amazonaws.com/zips.udacity-data.com/fd66c083-4ccb-4fe3-bda1-c29db76f50a0/199995/1500445794196/Vehicle%20Detection%20and%20Tracking%20Videos.zip)
* the models produced by the Exploration notebook

In order to run the SSD notebook, you will need:
* the pre-trained [weights](https://s3.amazonaws.com/zips.udacity-data.com/fd66c083-4ccb-4fe3-bda1-c29db76f50a0/199995/1500445794196/Vehicle%20Detection%20and%20Tracking%20Videos.zip)

## SVC Approach
### Feature Selection (Data Exploration Notebook)
The intention is to create a classifier capable of distinguishing between car and non-car image patches based upon measurable features.
The _car_ and _non-car_ classes are illustrated below.
![Car samples](/output_images/car_samples.png)

![Non-car samples](/output_images/non_car_samples.png)

In the realm of computer vision, there are a plethora of image characteristics that could be extracted that would assist in classification; the difficulty lies in discerning those that offer the most information.

One of the more popular methods for object recognition, despite its introduction over a decade ago, is the [Histogram of Oriented Gradients (HOG)](http://lear.inrialpes.fr/people/triggs/pubs/Dalal-cvpr05.pdf).  The algorithm attempts to describe a local objects appearance through a distribution of edge directions.  An image is divided into small connected regions known as _cells_ and a histogram of _gradient orientations_ is computed using all of the _pixels per cell_.  These local histograms are contrast normalised across a larger region, known as a _block_, which improves invariance to aspects such as illumination, shadowing and edge contrast.  Each cell is typically shared between different blocks but the normalisation is dependent on all cells in the block, hence each cell will appear several times in the output with different normalisations.

The image below illustrates the application of HOG on a channel basis across several color spaces.
![Hog sample](/output_images/hog_sample.png)

The HOG features primarily capture shape and this was additionally supplemented with some spatial domain features, captured through the image itself and histograms of each color channel.

With reference to the [Udacity project video](https://www.youtube.com/watch?v=P2zwrTM8ueA&feature=youtu.be) and through experimentation with different color spaces and models, the following final parameters were decided upon:
```
sc = SpatialC('YCrCb', # Color space for spatial domain
 True, 16, # Image resize
 True, 16, (0, 256) # Histogram size and bins per channel
 ) 

hc = HogC('YCrCb',True, # Color space for HOG
 9, # Number of orientations
 8, # Number of pixels per cell
 2, # Number of cells per block
 True, [0, 1, 2] # Channels to perform HOG on
 )
```
This leads to a feature vector with size:
```
(16 * 16 * 3) + (16 * 3) + (1764 * 3) = 768 + 48 + 5292 = 6108
```
HOG features while not the slowest descriptor to compute, are also nowhere near the fastest and in an effort to improve the feature extraction -> training cycle, the extraction process was parallelised.  In essence, a divide and conquer approach is adopted, with the results collated into a single array of features at the end.  The snippet below illustrates the key section.
```
def multiprocess_extraction(self, files):  
    ncpu = multiprocessing.cpu_count()  
    pool = multiprocessing.Pool(ncpu)  
    sublists = self._split_list(files, np.ceil(len(files)/ncpu).astype(np.int))  
    temp = pool.map(self.extract_features, sublists)  
    return [item for sublist in temp for item in sublist]
```
The feature extraction process was roughly six minutes for the sequential approach and reduced to roughly eighty seconds through the above.

### SVC Training (Data Exploration Notebook)
In the realm of SciKit, ***LinearSVC*** and ***SVC(kernel='linear')*** yield fundamentally different results, with the former having a *squared hinge* loss function by default and the latter *hinge* loss function.  Using squared loss technically means the former is solving more of a Regularized Least Squared (RLS) regression problem than classification.  The underlying estimators of the former are *liblinear* which penalise the intercept and are optimised for a linear case, leading to faster convergence on large amounts of data than the *libsvm* estimators used by the latter.

The data was split into two partitions, 85% and 15% respectively, and normalized using the *StandardScaler*.  

A couple of different approaches were adopted with regard to training the model using the above split data:
* LinearSVC:  Using the default parameters with a slightly lower tolerance, the accuracy of the model was in excess of 99%, trained in less than 20 seconds and could predict a result in milliseconds.  This model was generated a few times but when tried against the video when using a multi-scaled approach, there would always be a relatively large amount of false positives detected.
* SVC: There are a host of parameters that are available for optimisation.  A grid search approach with cross validations was used with both the Linear and Radial Basis Function (RBF) kernels, with the results for F1-score outlined in the table below.  The Linear kernel, regardless of the penalty term, produced superior results to the RBF during model generation and in later evaluation resulted in a much lower false positive detection rate than the LinearSVC but a far higher classification time. 

<center>

| Kernel | Penalty | Gamma | F1 Score | Deviation |
|:------:|:-------:|:-----:|:--------:|:---------:|
|   RBF  |    1    | 0.001 |  0.91723 | +/-0.0051 |
|   RBF  |    10   | 0.001 |  0.92384 | +/-0.0044 |
|   RBF  |   100   | 0.001 |  0.92384 | +/-0.0044 |
|   RBF  |   1000  | 0.001 |  0.92384 | +/-0.0044 |
| Linear |    1    |   -   |  0.98612 | +/-0.0040 |
| Linear |    10   |   -   |  0.98612 | +/-0.0040 |
| Linear |   100   |   -   |  0.98612 | +/-0.0040 |
| Linear |   1000  |   -   |  0.98612 | +/-0.0040 |

</center>

Prior to training, there was an additional attempt to use a dimensionality reduction technique known as Principle Component Analysis (PCA), reducing the size of the feature space to roughly half.  The accuracy of the generated models remained consistent and training time was far shorter but the computational time shot up exponentially, meaning it would be far less viable for a 'real-time' system.

### Sliding Window Search (SVM-Search Notebook)
The first approach consisted of generating all the windows for a particular scale and supplying them to a search function.  This function would take the indicated window from the image, perform feature extraction, scaling and subsequently classification, returning the window if it was classified as containing a car.  The results of both the LinearSVC and SVC are illustrated below. 

![Linear SVC sample](/output_images/linearsvc_sample.png)

![SVC sample](/output_images/svc_sample.png)

This approach did not scale up particularly well in terms of computation time, with the extraction of the HOG features on a per window basis being the main limiting factor.  Instead a function *(find_car_bboxs)* was implemented which would extract the HOG features across the entire image and the windows would extract their features from the descriptor computed across the entire image.  This markedly sped up computation but naturally leads to subtly different results, since there will be discontinuities when extracting the window.

In order to further improve the computational speed of the multi-scale approach, each scale is performed in parallel, illustrated below.
```
def find_car_bboxs_multi_p(img, scales, func, pool):
    bboxs = pool.map(partial(func, img=img), scales)
    return np.asarray([item for sublist in bboxs for item in sublist])
```
Naturally, this is not the best target for parallelisation, since the smallest scale will be the bottleneck in terms of computational time, but it was the low-hanging fruit and future efforts would be better spent in performing the windowing loop in parallel.  When using three scales, the computational time of the parallel approach proved roughly one third less than the sequential scaled approach, which was quite a large benefit.

### False Positive Suppression (SVM-Search Notebook)
The bounding boxes detected were summed together in the form of a heatmap.  The heatmap would subsequently be thresholded in an attempt to remove areas that were not detected by multiple windows, hence likely to be false positives.  Samples are offered below in addition to the noisiest detection, for both the LinearSVC and the SVC.

![Linear SVC Multiscale Heatmap](/output_images/linearsvc_ms_heatmap.png)

![Linear SVC Classification sample](/output_images/linearsvc_classification2.png)

![SVC Multiscale Heatmap](/output_images/svc_ms_heatmap.png)

![SVC Classification sample](/output_images/svc_classification2.png)

Due to the remaining false detections for the LinearSVC, a number of additional ad-hoc techniques were applied for the video:
* Heatmap buffer: The heatmap used when generating the bounding boxes was actually the sum over a small number of frames.  The sum was then thresholded based on the maximum of the mean heat or a static 6.0 through experimentation.
* Detection class: Every time a bounding box was detected, it would be compared against prior detections using the Intersection Of Union (IOU) method.  If it matched, the detection was marked as active for that frame and given the box.  Otherwise a new detection was created.  Each detection would have a small internal buffer of its location across a number of frames and it would mark itself as a car if it was updated consistently across a smaller number of frames.

Two methods were used for estimating the detection position across frames, with the SVM approach using a mean based estimation of the four bounding box co-ordinates.  The other estimation method offered was non-maximum suppression but this seemed to jump around excessively for the SVM detection.

The final results of the SVC approach are illustrated in videos [Linear SVC](/output_videos/rls.mp4) and [SVC](/output_videos/svc.mp4) respectively.  In terms of computation time, the Linear SVC approach is capable of running at roughly two frames per second whereas the SVC took over an hour to generate the indicated clip.  A number of false detections are also present in both approaches.  Both of these factors led to the trial of the SSD approach, which proved far more stable and capable of running in real-time when using a GPU.

## SSD Approach (SSD-Search Notebook)
The above approach has the potential to be highly accurate, if tuned a bit further, but appears too computationally intensive for embedded systems and likely too slow for real-time applications.

The SSD model is based on a feed-forward convolutional network producing bounding boxes and scores indicating the presence of certain object classes.  The model is complex, using VGG-16 as a base and supplementing it with multi-scale feature maps and convolutional predictors, and a Keras implementation compatible with the Udacity docker image was sourced from [rykov8/ssd_keras](https://github.com/rykov8/ssd_keras).  The pre-trained weights were additionally sourced from this repo since they were trained on the [Pascal Visual Object Classes (VOC)](http://host.robots.ox.ac.uk/pascal/VOC/), hence would be capable of classifying vehicles such as cars.

The model was similarly applied to the test images resulting in much more accurate positioning output of the vehicles from a single frame.  A confidence threshold of 20% was applied when producing the below.

![SSD classification](/output_images/ssd_classification.png)

The process for applying SSD to a video is similar to that of the SVC approach with respect to detections.  One notable adjustment is that non-maximum suppression is used when estimating the bounding box to draw rather than the mean-based approach.  This was inspired by the SSD paper where the technique is used for a single image post-classification; it made sense to similarly apply the technique on an inter-frame basis and it led to slightly more 'accurate' results than the mean based approach.  The output video is available [here](/output_videos/ssd.mp4). 

## Discussion
There are likely a large number of improvements that could be made to my attempt at using feature descriptors with a model such as SVC.
One of the more interesting of these would be the use of hard negative mining, whereupon the false positives detected during the video are directly fed into the *non-car* dataset and the classifier is re-trained.

One flaw with the former approach is performing the windowing and classification for every single frame.  An idea I've been toying with but not implemented is a hybrid approach interspersing detection with a robust motion tracking technique.  Tracking is naturally much faster since not much has changed between frames, given a suitably high FPS, and detection is effectively starting from scratch every frame.  Similarly, both the SVC and SSD approaches failed in the test video when one vehicle completely occluded the other; a good tracking algorithm would likely have been able to anticipate the position of the occluded object and produce two detections appropriately.  Due to the relatively high frame rate here, [median-flow](
http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.231.4285&rep=rep1&type=pdf) would likely prove viable here or one could play with a neural network based approach to tracking, such as [GoTurn](https://davheld.github.io/GOTURN/GOTURN.pdf).


