import cv2 
import numpy as np
import pickle
import time
from matplotlib import pyplot as plt

def readImage(fname):
    return cv2.cvtColor(cv2.imread(fname), cv2.COLOR_BGR2RGB)

def readImageAsDouble(fname):
    img = readImage(fname)
    info = np.iinfo(img.dtype)
    return img.astype(np.float32) / info.max
    
def save(obj, fname):
    with open(fname, 'wb') as out:
        pickle.dump(obj, out, pickle.HIGHEST_PROTOCOL)

def load(fname):
    with open(fname, 'rb') as inp:
        return pickle.load(inp)

def doWithTime(func):
    s = time.time()
    out = func()
    f = time.time()
    print('Time taken: ' + str(f - s)) 
    return out 

def visualise(fig, rows, cols, imgs, titles = None):
    for i, img in enumerate(imgs):
        ax = plt.subplot(rows, cols, i + 1)
        plt.imshow(img, cmap='hot' if len(img.shape) < 3 else None)
        if titles:
            ax.set_title(titles[i])

def draw_bboxes(img, bboxes, color=(0, 255, 0), thick=6):
    # Make a copy of the image
    imcopy = np.copy(img)
    # Iterate through the bounding boxes
    for bbox in bboxes:
        # Draw a rectangle given bbox coordinates
        cv2.rectangle(imcopy, (bbox[0], bbox[1]), (bbox[2], bbox[3]), color, thick)
    # Return the image copy with boxes drawn
    return imcopy

def draw_heatmap(img, bboxes):
    ret = np.zeros_like(img[:,:,0])
    for bbox in bboxes:
        ret[bbox[1]:bbox[3], bbox[0]:bbox[2]] += 1
    return ret

def apply_threshold(heatmap, threshold):
    # Zero out pixels below the threshold
    heatmap[heatmap <= threshold] = 0
    # Return thresholded map
    return np.clip(heatmap, 0, 255)

def draw_labeled_bboxes(img, labels):
    imcopy = np.copy(img)
    for car_number in range(1, labels[1]+1):
        # Find pixels with each car_number label value
        nonzero = (labels[0] == car_number).nonzero()
        # Identify x and y values of those pixels
        nonzeroy = np.array(nonzero[0])
        nonzerox = np.array(nonzero[1])
        # Define a bounding box based on min/max x and y
        bbox = ((np.min(nonzerox), np.min(nonzeroy)), (np.max(nonzerox), np.max(nonzeroy)))
                
        # Draw the box on the image
        cv2.rectangle(imcopy, bbox[0], bbox[1], (0,0,255), 6)
    # Return the image
    return imcopy

# Box functions
def area(box):
    return (box[2] - box[0] + 1) * (box[3] - box[1] + 1)

def areaOfIntersection(boxA, boxB):
    xA = max(boxA[0], boxB[0])
    yA = max(boxA[1], boxB[1])
    xB = min(boxA[2], boxB[2])
    yB = min(boxA[3], boxB[3])

    return (xB - xA + 1) * (yB - yA + 1)

# Intersection over union
def iou(boxA, boxB):
    interArea = areaOfIntersection(boxA, boxB)
    areaA, areaB = area(boxA), area(boxB)
    return interArea / np.float(areaA + areaB - interArea)

def estimateBoxes(labels):
    ret = []
    for car_number in range(1, labels[1]+1):
        # Find pixels with each car_number label value
        nonzero = (labels[0] == car_number).nonzero()
        # Identify x and y values of those pixels
        nonzeroy = np.array(nonzero[0])
        nonzerox = np.array(nonzero[1])
        # Define a bounding box based on min/max x and y
        box = (np.min(nonzerox), np.min(nonzeroy), np.max(nonzerox), np.max(nonzeroy))
        ret.append(box)
    return ret

def nonMaxSuppression(boxes, overlapThresh = 0.3):
    ret = []

    if len(boxes) != 0:
        if boxes.dtype.kind == "i":
            boxes = boxes.astype("float")

        pick = []

        x1 = boxes[:, 0]
        y1 = boxes[:, 1]
        x2 = boxes[:, 2]
        y2 = boxes[:, 3]

        # Compute area
        area = (x2 - x1 + 1) * (y2 - y1 + 1)
        # Sort by bry
        idxs = np.argsort(y2)

        while len(idxs) > 0:
            # grab the last index in the indexes list and add the
            # index value to the list of picked indexes
            last = len(idxs) - 1
            i = idxs[last]
            pick.append(i)

            # find the largest (x, y) coordinates for the start of
            # the bounding box and the smallest (x, y) coordinates
            # for the end of the bounding box
            xx1 = np.maximum(x1[i], x1[idxs[:last]])
            yy1 = np.maximum(y1[i], y1[idxs[:last]])
            xx2 = np.minimum(x2[i], x2[idxs[:last]])
            yy2 = np.minimum(y2[i], y2[idxs[:last]])

            # compute the width and height of the bounding box
            w = np.maximum(0, xx2 - xx1 + 1)
            h = np.maximum(0, yy2 - yy1 + 1)

            # compute the ratio of overlap
            overlap = (w * h) / area[idxs[:last]]

            # delete all indexes from the index list that have
            idxs = np.delete(idxs, np.concatenate(([last],
                                                   np.where(overlap > overlapThresh)[0])))

        # return only the bounding boxes that were picked using the
        # integer data type
        ret = boxes[pick].astype("int")

    return ret
