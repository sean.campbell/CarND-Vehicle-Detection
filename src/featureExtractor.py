import numpy as np                                                                                                    
import cv2
import multiprocessing
from skimage.feature import hog 
from src.misc import readImage

class FeatureExtractor():
    def __init__(self, spatial, hogc):
        self.spatial = spatial
        self.hogc = hogc

        self.cmapping = {'HSV':cv2.COLOR_RGB2HSV,'LUV': cv2.COLOR_RGB2LUV,
                         'YUV': cv2.COLOR_RGB2YUV,'HLS': cv2.COLOR_RGB2HLS,
                         'YCrCb':cv2.COLOR_RGB2YCrCb, 'LAB': cv2.COLOR_RGB2LAB}

    def get_channel_hog(self, channel, fv = True, vis = False):            
        return hog(channel, orientations = self.hogc.orient,
                                 pixels_per_cell = (self.hogc.ppc, self.hogc.ppc),
                                 cells_per_block = (self.hogc.cpb, self.hogc.cpb),
                                 block_norm = 'L2-Hys',
                                 transform_sqrt = False,
                                 visualise = vis,
                                 feature_vector = fv)

    def get_image_hog(self, img):
        hog_features = []
        for channel in self.hogc.channels:
            hog_features.append(self.get_channel_hog(img[:, :, channel]))
        return np.ravel(hog_features)

    def get_spatial(self, img):
        return cv2.resize(img, (self.spatial.size, self.spatial.size)).ravel()

    def get_channel_hist(self, channel):
        return np.histogram(channel, bins=self.spatial.nbins, range=self.spatial.bin_range)[0]

    def get_image_hist(self, img):
        return np.concatenate([self.get_channel_hist(img[:, :, idx]) for idx in range(0, img.shape[2])])

    def get_feature_image(self, img, cspace):
        ret = np.copy(img)
        cvt = self.cmapping.get(cspace, None)
        if cvt is not None:
            ret = cv2.cvtColor(ret, cvt)
        return ret
    
    def multiprocess_extraction(self, files):
        ncpu = multiprocessing.cpu_count()
        pool = multiprocessing.Pool(ncpu)
        sublists = self._split_list(files, 
                         np.ceil(len(files)/ncpu).astype(np.int))
        temp = pool.map(self.extract_features, sublists)
        return [item for sublist in temp for item in sublist]
        
    def extract_features(self, files):
        features = []
        for file in files:
            features.append(self.extract_image_features(readImage(file)))
        return features
        
    def extract_image_features(self, img):
        features = []
        if self.spatial.do or self.spatial.doHist:
            feature_img = self.get_feature_image(img, self.spatial.cspace)

            if self.spatial.do:
                features.append(self.get_spatial(feature_img))
            if self.spatial.doHist:
                features.append(self.get_image_hist(feature_img))

        if self.hogc.do:
            feature_img = self.get_feature_image(img, self.hogc.cspace)
            features.append(self.get_image_hog(feature_img))

        return np.concatenate(features)
    
    def get_shape(self, img):
        ret = []
        if self.spatial.do:
            ret.append(self.get_spatial(img).shape)
        if self.spatial.doHist:
            ret.append(self.get_image_hist(img).shape)
        if self.hogc.do:
            ret.append(self.get_image_hog(img).shape)
        return ret
    
    def _split_list(self, l, chunk_size):
        return [l[offs:offs+chunk_size] for offs in range(0, len(l), chunk_size)]

