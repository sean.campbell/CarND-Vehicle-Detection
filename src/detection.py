import numpy as np

from src.misc import area, iou, nonMaxSuppression


class Detection:
    def __init__(self, box, maxlen=5):
        self.maxlen = maxlen
        # Frame tracking
        self.activity = [True]

        # Box tracking
        self.estimate = box
        self.history = []

    def calcIOU(self, box):
        return iou(self.estimate, box)

    def updateBox(self, box):
        self.history.append(box)

        if len(self.history) > self.maxlen:
            self.history = self.history[1:]

        boxs = np.array(self.history)
        supBoxes = nonMaxSuppression(boxs)

        # Use the largest
        est = None
        biggest = 0.0
        for box in supBoxes:
            a = area(box)
            if a > biggest:
                est = box
                biggest = a

        self.estimate = est

    def updateBoxMean(self, box):
        self.history.append(box)

        if len(self.history) > self.maxlen:
            self.history = self.history[1:]

        boxs = np.array(self.history)
        supBoxes = nonMaxSuppression(boxs)

        # Use the largest
        est = None
        biggest = 0.0
        for box in supBoxes:
            a = area(box)
            if a > biggest:
                est = box
                biggest = a

        self.estimate = est

    def setFrameInactive(self):
        self.activity.append(False)
        if len(self.activity) > self.maxlen:
            self.activity = self.activity[1:]

    def setFrameActive(self):
        self.activity[-1] = True

    def isCar(self):
        return np.sum(self.activity) > (self.maxlen // 2)

    def canIgnore(self):
        return len(self.activity) == self.maxlen and np.mean(self.activity) < 0.2